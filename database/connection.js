const Sequelize = require('sequelize');
const connection = {};
console.log(`Connect to database ${process.env.MYSQL_USER}@${process.env.MYSQL_HOST}:${process.env.MYSQL_PORT}`)
const sequelize = new Sequelize(process.env.MYSQL_DATABASE, process.env.MYSQL_USER, process.env.MYSQL_PASSWORD, {
  host: process.env.MYSQL_HOST,
  port: process.env.MYSQL_PORT,
  dialect: "mysql",
  pool: {
    "max": 10,
    "min": 0,
    "acquire": 30000,
    "idle": 10000
  }
});

sequelize.sync({
  alter: false
});

connection.sequelize = sequelize;

module.exports = connection;
